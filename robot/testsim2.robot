*** Settings ***
Library  RemoteSwingLibrary
Library  AutoItLibrary
Library  SeleniumLibrary
Library  OperatingSystem

*** Variable ***
${item1}  112308
${item2}  183380

*** test case ***
test
    Set Selenium Speed  1s
    Open Browser  http://sml-uat-mom01.siammakro.co.th:28888/sim-uat/launch?template=sim_jnlp_template.vm  Chrome
    Input Text    css=#username  evitus
    Input Text    css=#password  W1c2FOHS
    Sleep  3s
    Click Element  css=#loginData > div.button-row > span > input
    Sleep  5s
    Mouse Click  LEFT  354  785
    sleep  3s
	Start Application  sim-uat  javaws C:\\Users\\doppi\\Downloads\\sim_jnlp_template.vm.jnlp  60  close_security_dialogs=True
    RemoteSwingLibrary.Select Window  Store Inventory Management
    # ${text} =  Get Selected Window Title
    # log to console  ${text}
    #select store
    ${title}=  Get Selected Window Title
    log to console  ${title}
   Mouse Click  LEFT  1063  93
   sleep  3s
   Mouse Move  1052  110
   sleep  3s
   Mouse Wheel  up  18
   sleep  3s
   Mouse Click  LEFT  1052  110
   sleep  3s
#Check inventory
   Mouse Click  LEFT  658  61
   sleep  3s
   Mouse Click  LEFT  530  61
   sleep  3s
   Mouse Click  LEFT  626  89
   sleep  3s
   Send  ${item1}
   # log to console  ${item1}

   Mouse Click  LEFT  478  60
   sleep  10s
   # Label Text Should Be  24  Store Inventory Management
   Mouse Click  LEFT  434  60
   sleep  5s



#Check inventory round 2
   Mouse Click  LEFT  626  89
   sleep  3s
   Send  ${item2}
   # log to console  ${item2}

   Mouse Click  LEFT  478  60
   sleep  10s
   # Label Text Should Be  172  Store Inventory Management
   Mouse Click  LEFT  434  60
   sleep  5s
#Go to main
   Mouse Click  LEFT  434  60
   sleep  5s
   Mouse Click  LEFT  434  60
   sleep  5s
#log out
   # Mouse Click  LEFT  1233  42
   Mouse Click  LEFT  801  62
   sleep  5s
   Close Browser
   # FileDelete  C:\Users\doppi\Downloads\sim_jnlp_template.vm.jnlp
   Remove File  C:\\Users\\doppi\\Downloads\\sim_jnlp_template.vm.jnlp
   log to console  Delete File Sim Success !