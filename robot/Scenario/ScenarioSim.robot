*** Settings ***
Library  SeleniumLibrary
Library  AutoItLibrary


Resource  ../Scenario/PageObject/Closesim.robot
Resource  ../Scenario/PageObject/Loginonweb.robot
Resource  ../Scenario/PageObject/Opensim.robot
Resource  ../Scenario/PageObject/Searchitem.robot
Resource  ../Scenario/PageObject/Selectstore.robot



*** Variable ***
${item1}  112308
${item2}  183380

*** KeyWord ***
Login
	Loginonweb.Login
	Sleep  3s
	Loginonweb.Click Keep
	Sleep  3s
	# Loginonweb.Click Open Sim
	# Sleep  3s

Run Sim
	Opensim.Click Run
	Sleep  3s

Select Store
	Selectstore.Click select store
	Sleep  3s

Search Item
	Searchitem.Click Lookitem
	Searchitem.Input Item Number  ${item1}
	Searchitem.Search Item
	Searchitem.Input Item Number  ${item2}
	Searchitem.Search Item

Close Sim
	Closesim.Click Back To Main
	Closesim.Click Logout Sim
	Closesim.Close Brower
    Closesim.Delete File



