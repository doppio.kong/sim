*** Settings ***
Resource  ../Scenario/ScenarioSim.robot


*** Test Cases ***
Run Sim
	ScenarioSim.Login
	ScenarioSim.Run Sim
	ScenarioSim.Select Store
	ScenarioSim.Search Item
	ScenarioSim.Close Sim
